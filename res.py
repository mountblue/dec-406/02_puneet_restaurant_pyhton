'''INPUT PARAMETERS

 date_time_str:str
        the formatted string with date and time as YYYY-MM-DD H:MM pm/am

  FUNCTIONS

 find_open_restaurants(csv_reader, date)
    finds the opened restaurant on the given day
  time_range()
    find the restaurant start and end time
  sync_time(start,end)
    to search the restaurant in given time
'''
import csv
import re
import datetime

date_time_str = input('enter the date in yyyy-mon-date hour:min am/pm format')
days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
with open('restaurant.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
date = datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M %p')
print(date)
print(date.weekday())
print(days[date.weekday()])
open_restaurant=[]
timing_restaurant=[]

#function to search for opened restaurant on the given day
def find_open_restaurants(csv_reader, date):
    day = days[date.weekday()]
    my_regex = r"\b" + re.escape(day) + r"\b"


    with open('restaurant.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        count=0
        for row in csv_reader:

            count=count+1
            for field in row:
                flag=0
                #if day name is directly in the field
                if re.search(day, field, re.IGNORECASE):
                    flag=1
                    open_restaurant.append(row[0])
                #if day is in range
                if flag != 1:
                    if row[1].count(',')==0:
                      start=row[1][0:3]

                      end=row[1][4:7]


                      k=int(days.index(start))
                      j=int(days.index(end))
                      for i in range(k,j):
                         if days[i]==day:
                             #saving the list of opened restaurant
                             open_restaurant.append(row[0])





#to find the time_range of restaurnant  in each row
def time_range():
    with open('restaurant.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        count = 0;

        for row in csv_reader:

            count=count+1
            open_timing=row[1].split('/')#split the row from/
            l=len(open_timing)
            for i in range(0,l):
                #if the row has only one -
                if open_timing[i].count('-')==1:
                    time_range=open_timing[i][4:].split('-')



                else:
                  if (open_timing[i].count(',')==0):
                        time_range = open_timing[i][8:].split('-')


                  else:
                    time_range = open_timing[i][13:].split('-')



            if time_range:
                start = time_range[0].strip()


                end = time_range[1].strip()

            if (sync_time(start,end)):
                #saving the list of opened restaurant in given time range
              timing_restaurant.append(row[0])



#to search the restaurant in given time
def sync_time(start,end):
     #only hours is given in start time
    if start.count(':') == 0:

        start = datetime.datetime.strptime(start, "%I %p")
     #if hour:minute is given in start time
    else:

        start = datetime.datetime.strptime(start, "%I:%M %p")
     # only hours is given in end time
    if end.count(':') == 0:
        end = datetime.datetime.strptime(end, "%I %p")

    # if hour:minute is given in end time
    else:
        end = datetime.datetime.strptime(end, "%I:%M %p")

    #fetching the user time from the input string in 24hrs time
    user_timing = datetime.datetime.strptime(date_time_str[11:], "%I:%M %p")

    # restaurant opened at midnight
    if start > end:
        if user_timing<=end:
            return True
    #dattime
    if user_timing >= start and user_timing <= end:


        return True
    else:
        return False




find_open_restaurants(csv_reader, date)
time_range()

#copying both list in set so comparison will be easy
seta=set(timing_restaurant)
setb=set(open_restaurant)

#printing intersection in both the sets
if(seta & setb):


      l=list(seta & setb)
for i in range(0,len(l)):
    print(l[i])